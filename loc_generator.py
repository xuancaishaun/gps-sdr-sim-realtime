import socket
import time

UDP_IP = "127.0.0.1"
UDP_PORT = 5005
lat = 47.653700
lon = -122.306306
hgt = 10.0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

count = 0
step = 0.000005
while True:
    loc = "0, {}, {}, {}".format(lat+count*step, lon, hgt)
    count += 1
    ret = sock.sendto(bytes(loc, "utf-8"), (UDP_IP, UDP_PORT))
    print("Next loc = ({}), ret value = {}".format(loc, ret))
    time.sleep(1)
