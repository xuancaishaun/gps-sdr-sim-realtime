#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tcp Top Block
# Generated: Fri Aug 10 18:51:34 2018
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import time


class tcp_top_block(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Tcp Top Block")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 2500000
        self.gain = gain = 0
        self.frequency = frequency = 1575420000

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("serial=3136D6B", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_clock_source('internal', 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(frequency, 0)
        self.uhd_usrp_sink_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_sink = uhd.usrp_sink(
        	",".join(("serial=3150311", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink.set_clock_source('internal', 0)
        self.uhd_usrp_sink.set_samp_rate(samp_rate)
        self.uhd_usrp_sink.set_center_freq(frequency, 0)
        self.uhd_usrp_sink.set_gain(gain, 0)
        self.uhd_usrp_sink.set_antenna('TX/RX', 0)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((0.001, ))
        self.blocks_multiply_const_vxx = blocks.multiply_const_vcc((0.001, ))
        self.blocks_interleaved_short_to_complex_0 = blocks.interleaved_short_to_complex(False, False)
        self.blocks_interleaved_short_to_complex = blocks.interleaved_short_to_complex(False, False)
        self.blks2_tcp_source_0_0 = grc_blks2.tcp_source(
        	itemsize=gr.sizeof_short*1,
        	addr='127.0.0.1',
        	port=1235,
        	server=True,
        )
        (self.blks2_tcp_source_0_0).set_max_output_buffer(20000)
        self.blks2_tcp_source_0 = grc_blks2.tcp_source(
        	itemsize=gr.sizeof_short*1,
        	addr='127.0.0.1',
        	port=1234,
        	server=True,
        )
        (self.blks2_tcp_source_0).set_max_output_buffer(20000)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blks2_tcp_source_0, 0), (self.blocks_interleaved_short_to_complex, 0))
        self.connect((self.blks2_tcp_source_0_0, 0), (self.blocks_interleaved_short_to_complex_0, 0))
        self.connect((self.blocks_interleaved_short_to_complex, 0), (self.blocks_multiply_const_vxx, 0))
        self.connect((self.blocks_interleaved_short_to_complex_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_multiply_const_vxx, 0), (self.uhd_usrp_sink, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink.set_samp_rate(self.samp_rate)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0.set_gain(self.gain, 0)

        self.uhd_usrp_sink.set_gain(self.gain, 0)


    def get_frequency(self):
        return self.frequency

    def set_frequency(self, frequency):
        self.frequency = frequency
        self.uhd_usrp_sink_0.set_center_freq(self.frequency, 0)
        self.uhd_usrp_sink.set_center_freq(self.frequency, 0)


def main(top_block_cls=tcp_top_block, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
