#!/usr/bin/env python
# -*-coding:utf-8-*-
import multiprocessing
import socket
import os
from motionless import AddressMarker, DecoratedMap, CenterMap, VisibleMap

# import struct
# import time
# import web
# import csv
# import googlemaps
# from datetime import datetime
import requests
import webbrowser
import threading
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

BUF_SIZE = 1024
UDP_IP = "127.0.0.1"
UDP_PORT = 5006
sentinel = -1


G_MAPS_URL = 'https://maps.googleapis.com/maps/api/staticmap'
trajectory_file = '../circle.csv'

# WGS-84 Constants
a = 6378137  # Semi-major radius (m)
b = 6356752.3142  # Semi-minor radius (m)
g0 = 9.7803267714  # Equatorial gravity (m/s^2)
k = 0.00193185138639  # Equatorial gravity constant (.)
e2 = 0.00669437999013  # Eccentricity squared (.)


class UDPServer(object):
    def __init__(self, host, port):
        # type: (object, object) -> object
        self._host = host
        self._port = port
        self.lat = 47.653700
        self.lon = -122.306306
        self.hgt = 10.0

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self._host, self._port))
        print('---> Starting UDP server on port ', self._port)

    def receive_message(self, data_collection_thread, queue):
        while not data_collection_thread.is_set():

            data, addr = self.sock.recvfrom(BUF_SIZE)
            data = str(data).split(',')

            ts = float(str(data[0]).split('\'')[1])
            self.lat = float(str(data[1]).split(' ')[1])
            self.lon = float(str(data[2]).split(' ')[1])
            temp = str(data[3]).split(' ')[1]
            self.hgt = float(temp.split('\'')[0])

            queue.put([ts, self.lat, self.lon, self.hgt])

        self.sock.close()


def real_time_map(queue, onetime):

    while True:
        data = queue.get()
        cmap = CenterMap(lat=data[1], lon=data[2])

        htmlPage = """
            <html>
                <body>
                    <h2>Generated Location</h2>
                    <img src="%s"/>
                </body>
            </html>
        """ % cmap.generate_url()

        with open("demo.html", "w") as html:
            html.write(htmlPage)
        print("demo.html created")
        # onetime = False

        try:
            print(data)
        except KeyboardInterrupt:
            return


def main():

    onetime = True
    queue = multiprocessing.Queue()

    s = UDPServer(UDP_IP, UDP_PORT)

    mapping_process = multiprocessing.Process(target=real_time_map, args=(queue, onetime, ))

    data_collection_thread = threading.Event()
    rx_thread = threading.Thread(target=s.receive_message, args=(data_collection_thread, queue))
    rx_thread.start()
    mapping_process.start()






    input("---> Type any key to quit:\n")
    data_collection_thread.set()

    print("---> Stopping UDP thread ...")
    rx_thread.join()

    try:
        mapping_process.join()
    except KeyboardInterrupt:
        mapping_process.terminate()

    print("\n---> Process complete.")



        # p_req = requests.PreparedRequest()
        # p_req.prepare_url(G_MAPS_URL, {'size':'800x500', 'markers': coords})
        # webbrowser.open(p_req.url)


if __name__ == "__main__":
    main()

