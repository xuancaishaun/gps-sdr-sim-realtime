This is the system architecture of our GPS spoofing experiments.


                     192.168.1.115:2235  192.168.1.115:1235
    --------------     --------------      --------------                              -----------------
    | Trajectory | ___ | Simulator  | ____ | Simulator  | ____                    _____| Target GPS Rx |
    | Generator  |     |   Engine   |      |    USRP    |     |                   |    -----------------
    --------------     --------------      --------------     |                   |
  Authentic trajectory                                        |    ------------   |
                                                              |____| Combiner |___|    (Assuming co-located)
  Spoofed trajectory                                          |    |          |   |
  192.168.1.115:5020  192.168.1.115:2234  192.168.1.115:1234  |    ------------   |
    --------------     --------------      --------------     |                   |    -----------------
    |  Spoofer   | ___ |   Spoofer  | ____ |   Spoofer  | ____|                   -----|  Ref GPS Rx   |
    |  Console   |     |   Engine   |      |    USRP    |                              -----------------
    --------------     --------------      --------------                                      |
          |                                                                                    |
          |                                                                                    |
          --------------------------------------------------------------------------------------
                                               Actual trajectory

Notes:
1. We assume that the reference GPS Rx (owned by the attacker) is close to the target GPS Rx.
2. Since we do not have a second combiner at this moment, we assume the target and reference GPS Rx are the same.