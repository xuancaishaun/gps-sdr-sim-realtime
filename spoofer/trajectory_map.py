import datetime
import dash
from dash.dependencies import Input, Output, State, Event
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
from plotly import graph_objs as go
from plotly.graph_objs import *

from flask import Flask
import pandas as pd
import numpy as np
import os

import socket
import threading
import socketserver



mapbox_access_token = 'pk.eyJ1Ijoiam1hemVyIiwiYSI6ImNqazAyMWVuYTAyYm4za3F5MHhwMXg4bGcifQ.vpFwKwxSrTZpOiqXcmF_bg'
app = dash.Dash(__name__)
server = app.server
app.layout = html.Div(
                html.Div([
                dcc.Graph(id='live-update-graph'),
                dcc.Interval(id='interval-component', interval=1*1000, n_intervals=0)
                ]),
    )


class ThreadedUDPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        self.server.process_udp_packets(data.decode())

class ThreadedUDPServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
    def __init__(self, server_address, RequestHandlerClass, process_udp_packets):
        self.process_udp_packets = process_udp_packets
        super().__init__(server_address, RequestHandlerClass)

class TrajectoryMapper:
    def __init__(self):
        self.host = '192.168.1.115'
        self.port = 5050
        self.server = None
        self.msg_count = 0
        self.rpi_status = 0

        self.df = pd.DataFrame(columns=['rx_lat', 'rx_lon', 'spoof_lat', 'spoof_lon', 'auth_lat', 'auth_lon'])

        self.rpi_llh = [47.653700,-122.306306,10]
        self.spoof_llh = [47.653700,-122.306306,10]
        self.auth_llh = [47.653700,-122.306306,10]

    def start(self):
        self.server = ThreadedUDPServer((self.host, self.port), ThreadedUDPRequestHandler, self.process_udp_packets)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print("TrajectoryMapper: {}:{}".format(self.host, self.port))

    def process_udp_packets(self, msg):
        msg = msg.split(',')
        id = msg[0]
        print(msg)

        if len(msg) == 4:
            llh = [float(msg[1]), float(msg[2]), float(msg[3])]
        else:
            llh = [float(msg[1]), float(msg[2]), 10.0]


        if id == 'ref_gps_rx':
            self.rpi_llh = llh
        elif id == 'spoofed':
            self.spoof_llh = llh
        elif id == 'authentic':
            self.auth_llh = llh


        self.df = self.df.append({
            'rx_lat': self.rpi_llh[0],
            'rx_lon': self.rpi_llh[1],
            'spoof_lat': self.spoof_llh[0],
            'spoof_lon': self.spoof_llh[1],
            'auth_lat': self.auth_llh[0],
            'auth_lon': self.auth_llh[1]
        }, ignore_index=True)
        self.msg_count += 1


tm = TrajectoryMapper()

def layout():
    return html.Div(
                html.Div([
                dcc.Graph(id='live-update-graph'),
                dcc.Interval(id='interval-component', interval=1*1000, n_intervals=0)
                ]),
    )


def generate_map():
    update_data = [
        # go.Scattermapbox(lon=tm.df['spoof_lon'], lat=tm.df['spoof_lat'], mode='markers', marker=dict(size=8, color='red')),
        go.Scattermapbox(lon=tm.df['auth_lon'], lat=tm.df['auth_lat'], mode='markers', marker=dict(size=8, color='blue')),
        go.Scattermapbox(lon=tm.df['rx_lon'], lat=tm.df['rx_lat'], mode='markers', marker=dict(size=6, color='black')),
    ]
    update_layout = go.Layout(
        height=800,
        hovermode='closest',
        showlegend=False,
        mapbox=dict(
            accesstoken=mapbox_access_token,
            bearing=0,
            center={'lon': tm.spoof_llh[1], 'lat': tm.spoof_llh[0]},
            style='light',
            zoom=12,
        ),
    )
    return {'data': update_data, 'layout': update_layout}


@app.callback(Output('live-update-graph', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_map(n):
    return generate_map()


if __name__ == '__main__':
    tm.start()
    app.run_server(debug=False)
