# This script generates simulated authentic GPS locations for the target GPS Rx.

import socket
import time, sched
import matplotlib.pyplot as plt
import numpy as np
from utility import *
from config import *


class TrajectorySimulator:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.scheduler = sched.scheduler(time.time, time.sleep)
        self.duration_sec = None
        self.curr_time = 0
        self.mode = 'Line'
        self.msg_counter = 0
        self.lat_hist = []
        self.lon_hist = []
        self.start_time = time.time()

    def start(self, initial_llh=(47.653700, -122.306306, 10.0), speed_km_sec=0.005,
              bearing_deg=0, interval_sec=1, duration_sec=None, plot_flag=False):
        self.duration_sec = duration_sec
        self.plot_flag = plot_flag

        print('Simulator started: pos = {}, speed = {} km/sec, bearing = {} degrees'.format(
            initial_llh, speed_km_sec, bearing_deg
        ))

        if self.mode == 'Line':
            #print('Initial position is {}'.format(initial_pos))
            self.send(initial_llh)
            self.lat_hist.append(initial_llh[0])
            self.lon_hist.append(initial_llh[1])

            dist_km = speed_km_sec * interval_sec
            next_llh = compute_destination(initial_llh, dist_km, bearing_deg)
            self.curr_time += interval_sec
            self.scheduler.enter(interval_sec, 1, self.generate, argument=(next_llh, speed_km_sec, interval_sec, bearing_deg))
            self.scheduler.run()
        else:
            print('Unknown mode for trajectory generation')

    # This function generates locations that correspond to a straight line.
    def generate(self, curr_llh, speed_km_sec, interval_sec, bearing_deg):
        #print('Current position is {}'.format(curr_pos))
        self.send(curr_llh)
        self.lat_hist.append(curr_llh[0])
        self.lon_hist.append(curr_llh[1])

        event = None

        if self.mode == 'Line':
            dist_km = speed_km_sec * interval_sec
            next_llh = compute_destination(curr_llh, dist_km, bearing_deg)
            next_speed_km_sec = speed_km_sec
            next_interval_sec = interval_sec
            next_heading_deg = bearing_deg

            event = self.scheduler.enter(interval_sec, 1, self.generate,
                                         argument=(next_llh, next_speed_km_sec, next_interval_sec, next_heading_deg))
            self.curr_time += interval_sec
        else:
            print('Unknown mode for trajectory generation')

        if (self.duration_sec != None):
            if (self.curr_time >= self.duration_sec) & (event != None):
                self.scheduler.cancel(event)
                print('==> Stopping: total duration = {} sec'.format(self.duration_sec))

    def send(self, llh):
        msg = '{} {} {} {} {}'.format("authentic", self.get_elapsed_time(), llh[0], llh[1], llh[2])
        print("[{}], Dst={}, msg='{}'".format(self.msg_counter, SIMULATOR_ENGINE_ADDR, msg))
        self.sock.sendto(bytes(msg, "utf-8"), SIMULATOR_ENGINE_ADDR)
        self.msg_counter += 1

        # This is for demo purposes only
        self.send_to_spoofer_console(llh)

    def send_to_spoofer_console(self, llh):
        msg = '{}, {}, {}, {}, {}, {}, {}'.format('authentic', self.get_elapsed_time(), llh[0], llh[1], llh[2], 3, 0.0) # fix: 3
        self.sock.sendto(bytes(msg, "utf-8"), SPOOFER_CONSOLE_ADDR)

        if MODE == MODE_TEST:
            lat_noise = float(np.random.randn(1)*1e-6)    # in the order of meters
            lon_noise = float(np.random.randn(1)*1e-6)    # in the order of meters
            msg = '{}, {}, {}, {}, {}, {}, {}'.format('ref_gps_rx', self.get_elapsed_time(),
                                                      llh[0]+lat_noise, llh[1]+lon_noise, llh[2], 3, 0.0)  # fix: 3
            self.sock.sendto(bytes(msg, "utf-8"), SPOOFER_CONSOLE_ADDR)

    def get_elapsed_time(self):
        return round(time.time() - self.start_time, 3)

    def shutdown(self):
        print("Stopping trajector simulator")
        self.sock.close()
        print("# msgs sent = {}".format(self.msg_counter))

        if self.plot_flag:
            plt.plot(self.lon_hist, self.lat_hist, color='red', marker='x',
                     linestyle='dashed', linewidth=1)
            plt.title('Simulated trajectory')
            plt.xlabel('Longitude')
            plt.ylabel('Latitude')
            plt.show()

if __name__ == '__main__':
    trajectory_simulator = TrajectorySimulator()

    initial_llh = (47.653700, -122.306306, 10.0)
    speed_km_sec = SPEED_KM_PER_SEC
    bearing_deg = 0

    try:
        trajectory_simulator.start(initial_llh=initial_llh, speed_km_sec=speed_km_sec, bearing_deg=bearing_deg,
                                   duration_sec=None, plot_flag=False, interval_sec=0.1)
    except KeyboardInterrupt:
        trajectory_simulator.shutdown()
