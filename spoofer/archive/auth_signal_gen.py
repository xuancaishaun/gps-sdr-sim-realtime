import socket
import time

UDP_IP = "127.0.0.1"
UDP_PORT = 2235
true_loc_port = 5020

lat = 47.653700
# 47.653700
# 47.653695
lon = -122.306306
# -122.306306
# -122.306289
hgt = 10.0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

count = 0
step = 0.000005

while True:
    try:
        # loc = "0, {}, {}, {}".format(lat, lon, hgt) # STATIC
        loc = "0, {}, {}, {}".format(lat+count*step, lon, hgt) # DYNAMIC

        temp = "auth, 0, {}, {}, {}".format(lat+count*step, lon, hgt) # sent to spoofer for current location
        count += 1

        ret = sock.sendto(bytes(loc, "utf-8"), (UDP_IP, UDP_PORT))
        n_loc = sock.sendto(bytes(temp, "utf-8"), ('192.168.1.115', true_loc_port))

        print("\tNext loc = ({}), ret value = {}".format(loc, ret))
        time.sleep(1)
    except KeyboardInterrupt:
        sock.close()
        break
