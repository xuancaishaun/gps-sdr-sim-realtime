#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import threading
import socketserver
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.console
import time
import numpy as np
from matplotlib.figure import Figure
import pandas as pd
import plotly.plotly as py
from plotly.graph_objs import *
import csv


HOST = '192.168.1.115'  # '127.0.0.1'   # UDP socket address of the spoofer
PORT = 5020  # 5005          # For receiving and transmitting UDP packets

gps_sdr_sim_IP = '192.168.1.115'  # '127.0.0.1'
gps_sdr_sim_port = 2234  # 5010

usrp_IP = '192.168.1.115'  # usrp_IP = '127.0.0.1'
usrp_port = 5015
#
# true_loc_ip = '192.168.1.115'
# true_loc_port = 5555

trajectory_map_IP = '192.168.1.115'
trajectory_map_port = 5050



x_data = dict()
y_data = dict()
curves = dict()

x_data['victim'] = []
y_data['victim'] = []
curves['victim'] = dict()

x_data['spoofer'] = []
y_data['spoofer'] = []
curves['spoofer'] = dict()

x_data['authentic'] = []
y_data['authentic'] = []
curves['authentic'] = dict()
csv_counter = 0

data = dict()
data['rx_lat'] = []
data['rx_lon'] = []
data['spoofer_lat'] = []
data['spoofer_lon'] = []
data['authentic_lat'] = []
data['authentic_lon'] = []


# https://docs.python.org/3.6/library/socketserver.html
# https://gist.github.com/arthurafarias/7258a2b83433dfda013f1954aaecd50a
class ThreadedUDPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        # print("{} wrote:".format(self.client_address[0]))
        self.server.process_udp_packets(data.decode())


class ThreadedUDPServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
    def __init__(self, server_address, RequestHandlerClass, process_udp_packets):
        # type: (object, object, object) -> object
        self.process_udp_packets = process_udp_packets
        super().__init__(server_address, RequestHandlerClass)


class Spoofer:
    def __init__(self, host='127.0.0.1', port=5005):
        # type: (object, object) -> object
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server = None
        self.gps_sdr_sim_addr = None
        self.usrp_addr = None
        self.trajectory_map_addr = None

        self.rpi_msg = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # [timestamp, lat, lon, height, fix-type, elapsed_time]
        self.rpi_msg_last_lock = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # [timestamp, lat, lon, height, fix-type, elapsed_time]
        self.spoofer_msg = [0.0, 0.0, 0.0, 0.0]  # timestamp, lat, lon, height
        self.authentic_msg = [0.0, 0.0, 0.0, 0.0]  # timestamp, lat, lon, height

        # self.spoofer_msg_list = []
        # self.authentic_msg_list = []
        # self.rpi_msg_list = []

        self.lat_constant = 0.0
        self.lon_constant = 0.0
        self.hgt_constant = 0.0

        self.timer = False
        self.attack_timer_start = 0.0
        self.lock_timer_start = 0.0
        self.global_timer_end = 0.0
        self.v_step = 0.000005  # 0.000005

        self.starting_auto_spoof = False
        self.auto_spoof_commands = ['n 500 g 15', 'g 20', 'g 25', 'g 30', 'g 35', 'n 100', 'g 40', 'g 42', 'g 45',
                                    's 1']

        self.AL = False  # authentic lock status
        self.SL = False  # spoofing lock status

        self.standby = True  # standby mode status
        self.attack = False  # attack mode status

        self.attack_counter = False
        self.attack_count = 1

        self.lost_lock = False
        self.deviation = False

        self.lat_vel = 0.0
        self.lon_vel = 0.0
        self.hgt_vel = 0.0
        self.lat_vel_AUTH = 0.0

    def start(self):
        self.server = ThreadedUDPServer((self.host, self.port), ThreadedUDPRequestHandler, self.process_udp_packets)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        print("Spoofer started at {} port {}".format(self.host, self.port))

    def set_gps_sdr_sim_addr(self, addr):
        self.gps_sdr_sim_addr = addr

    def set_usrp_addr(self, addr):
        self.usrp_addr = addr

    def set_trajectory_map_addr(self, addr):
        self.trajectory_map_addr = addr

    def process_udp_packets(self, msg):
        msg = msg.split(',')
        identifier = msg[0]
        # print(msg)

        if identifier == 'rpi':
            self.rpi_msg = [
                float(msg[1]),  # gps_timestamp
                float(msg[2].split(' ')[1]),  # latitude
                float(msg[3].split(' ')[1]),  # longitude
                float(msg[4].split(' ')[1]),  # height
                int(msg[5].split(' ')[1]),  # fix-type
                float(msg[6].split(' ')[1])  # receiver_elapsed_time
            ]

            # STANDBY MODE: RX is locked onto the authentic signals
            if self.standby:
                if self.rpi_msg[4] == 3:
                    self.AL = True
                    self.SL = False
                    self.lat_constant = self.rpi_msg[1]
                    self.lon_constant = self.rpi_msg[2]
                    self.hgt_constant = self.rpi_msg[3]
                else:
                    self.AL = False
                    self.SL = False

            # ATTACK MODE: auto attack sequence started/in-progress
            if self.attack:
                if self.rpi_msg[4] == 3:  # AL | SL = True
                    pass
                elif self.rpi_msg[4] == 0:
                    self.AL = False
                    self.SL = False
                    self.lost_lock = True

            if self.timer:
                self.check_fix_status(self.rpi_msg[5], self.rpi_msg[4])  # (elapsed_time, fix-type)

            if self.attack_counter:
                self.attack_count += 1
            # else:
            #     self.attack_count = 1

            self.update_location(self.rpi_msg)  # spoofer updates the coordinates for signal
            self.send("rpi,{},{},{},{}".format(self.rpi_msg[1], self.rpi_msg[2], self.rpi_msg[3], self.rpi_msg[4]), self.trajectory_map_addr)


        elif identifier == 'auth':
            self.authentic_msg = [
                0,
                float(msg[2].split(' ')[1]),  # latitude
                float(msg[3].split(' ')[1]),  # longitude
                float(msg[4].split(' ')[1]),  # height
            ]
            self.send("auth,{},{},{},{}".format(self.authentic_msg[1],self.authentic_msg[2],self.authentic_msg[3],0), self.trajectory_map_addr)


    # Check fix-status of receiver during attack to see when it locks onto a signal
    def check_fix_status(self, t_el, fix):

        if (fix == 3) & self.lost_lock:  # if 3D lock, and already previously lost-lock
            self.timer = False
            self.global_timer_end = t_el

            lol = self.global_timer_end - self.lock_timer_start  # loss-of-lock timer
            total_dur = self.global_timer_end - self.attack_timer_start  # total attack timer

            print("..... Loss-of-Lock:  {0:.3f} sec".format(lol))
            print("..... Duration:      {0:.3f} sec".format(total_dur))

            self.global_timer_end = 0.0  # reset timer values
            self.lock_timer_start = 0.0
            self.attack_timer_start = 0.0

            self.adjust_power('n', 0)  # normalize the spoofing signal
            self.adjust_power('g', 0)

            # reset attack flags/variables
            if self.attack:
                self.attack = False
                self.attack_counter = False
                self.SL = True
                self.attack_count = 0
        else:
            if self.starting_auto_spoof:
                print("-----> STARTING ATTACK")
                self.starting_auto_spoof = False
            print(".....")

    # Auto-spoof commands iterated through
    def auto_spoof(self):
        self.attack_timer_start = self.rpi_msg[5]

        for index in range(0, len(self.auto_spoof_commands) - 2):
            cm = self.auto_spoof_commands[index].split(' ')

            if len(cm) == 4:
                self.adjust_power(cm[0], cm[1])
                self.adjust_power(cm[2], cm[3])
            else:
                self.adjust_power(cm[0], cm[1])

            time.sleep(0.3)

        self.timer = True
        self.lock_timer_start = self.rpi_msg[5]
        s_cm = self.auto_spoof_commands[len(self.auto_spoof_commands) - 1].split(' ')
        self.adjust_power(s_cm[0], s_cm[1])

    def process_user_input(self, msg):
        output = 'User input: {}'.format(msg)
        x = msg.split(' ')

        if msg == 'a':
            output = 'ATTACK: starting spoofing attack ...'
            self.starting_auto_spoof = True
            self.lost_lock = False
            self.standby = False
            self.attack = True
            self.deviation = False
            self.attack_counter = True
            self.auto_spoof()


        elif msg == 'r':
            output = 'RESET: all signal parameters back to default values ...'
            self.adjust_power('g', 0)
            self.adjust_power('n', 0)
            self.adjust_power('s', 0)

            self.standby = True
            self.attack = False
            self.deviation = False
            self.timer = False
            self.lost_lock = False

        elif msg == 'd':
            output = 'DEVIATION: changing victim location ...'
            self.standby = False
            self.attack = True
            self.deviation = True


        elif msg == 'ls':
            self.send("{},{},{}".format('ls', 0.0, self.rpi_msg[5]), self.usrp_addr)

        elif msg == 'n':
            output = 'NORMAL: setting spoofing noise = 0, and hardware gain = 0'
            self.adjust_power('n', 0)
            self.adjust_power('g', 0)

        # ----- this can probably be cleaned up, just hardcoded it to add it really fast
        elif len(x) == 4:
            target1 = x[0]
            target2 = x[2]
            value1 = x[1]
            value2 = x[3]
            self.adjust_power(target1, value1)
            self.adjust_power(target2, value2)

        elif len(x) == 6:
            target1 = x[0]
            target2 = x[2]
            target3 = x[4]
            value1 = x[1]
            value2 = x[3]
            value3 = x[5]
            self.adjust_power(target1, value1)
            self.adjust_power(target2, value2)
            self.adjust_power(target3, value3)
        # ------------------------------------

        elif len(x) != 2:
            output += "*** ERROR: incorrect format ***"

        else:
            target = x[0]
            value = x[1]

            if target == 's':  # spoofing signal state (toggle ON/OFF)
                if int(value) >= 1:
                    self.adjust_power(target, 1)
                else:
                    self.adjust_power(target, 0)
            elif target == 'g':  # USRP output gain
                output = 'Hardware Gain = {} '.format(value)
                self.adjust_power(target, value)
            elif target == 'n':  # noise state/value
                output = 'Noise Multiplier = {} '.format(value)
                self.adjust_power(target, value)
            elif target == 'a':  # noise amplitude
                output = 'Noise Amplitude = {} '.format(value)
                self.adjust_power(target, value)
            else:
                output += 'ERROR: Unknown command'
        return output

    # Update spoofer coordinates for signal generator
    def update_location(self, loc):

        if self.deviation:
            self.spoofer_msg = self.deviation_location_update(loc)

        elif self.attack:
            self.spoofer_msg = self.attack_location_update(loc)

        elif self.SL:  # Spoofing lock = True
            self.spoofer_msg = self.spoof_lock_location_update(loc)

        elif self.standby:
            self.spoofer_msg = self.standby_location_update(loc)

        self.send("spoof,{},{},{},{}".format(self.spoofer_msg[1],self.spoofer_msg[2],self.spoofer_msg[3],0), self.trajectory_map_addr)

    # return [lat_vel, lon_vel, hgt_vel, time_delta] from most recent lock to current loc
    def get_rx_velocity(self, loc):
        time_delta = float((loc[0] - self.rpi_msg_last_lock[0]) / 1e3)
        # lat_delta = float(loc[1] - self.rpi_msg_last_lock[1])
        # lon_delta = float(loc[2] - self.rpi_msg_last_lock[2])
        # hgt_delta = float(loc[3] - self.rpi_msg_last_lock[3])

        self.lat_vel = 1.0  # lat_delta/time_delta
        self.lon_vel = 1.0  # lon_delta/time_delta
        self.hgt_vel = 1.0  # hgt_delta/time_delta

        return [self.lat_vel, self.lon_vel, self.hgt_vel,
                time_delta]  # velocity in latitude direction only for simplicity

    # match to coordinates being sent from authentic to victim receiver, (spoofer not transmitting yet)
    def standby_location_update(self, loc):
        lat = loc[1]
        lon = loc[2]
        hgt = 10.0  # loc[3]
        fix = loc[4]
        init_llh = [0, 47.653700, -122.306306, 10.0]

        if fix == 3:
            rx_vel = self.get_rx_velocity(loc)  # rate of change from last LLH to current LLH
            self.rpi_msg_last_lock = loc  # update most recent 3D-fix LLH

            # v_lat = rx_vel[0]
            # v_lon = rx_vel[1]
            # v_hgt = rx_vel[2]
            elapsed_time = rx_vel[3]

            new_lat = lat + (self.v_step * (self.attack_count * elapsed_time))

            spoofer_msg = [0, new_lat, lon, hgt]
            self.send("{},{},{},{}".format(0, new_lat, lon, hgt), self.gps_sdr_sim_addr)
            # self.send("{},{},{},{}".format(0, new_lat, lon, hgt), self.trajectory_map_addr)
            print("STANDBY (fix = 3D-A) {}, {}, {}, {}".format(0, new_lat, lon, hgt))
            return spoofer_msg

        else:  # output default LLH until 3D-A fix
            spoofer_msg = [init_llh[0], init_llh[1], init_llh[2], init_llh[3]]
            self.send("{},{},{},{}".format(init_llh[0], init_llh[1], init_llh[2], init_llh[3]), self.gps_sdr_sim_addr)
            print("STANDBY (fix = n/a): {}, {}, {}, {}".format(init_llh[0], init_llh[1], init_llh[2], init_llh[3]))
            return spoofer_msg

    # Spoofer LLH during attack, when RX doesn't have a new fix
    def attack_location_update(self, loc):
        fix = loc[4]

        if fix != 3:  # (lock = authentic or NONE)
            ll_ts = self.rpi_msg_last_lock[0]
            ll_lat = self.rpi_msg_last_lock[1]
            # ll_lon = self.rpi_msg_last_lock[2]
            ll_hgt = 10.0

            lat = ll_lat + (self.v_step * self.attack_count)  # lat + (constantVelocityStep * count)
            self.attack_count += 1

            spoofer_msg = [ll_ts, lat, self.lon_constant, ll_hgt]
            self.send("{},{},{},{}".format(ll_ts, lat, self.lon_constant, ll_hgt),
                      self.gps_sdr_sim_addr)  # use previous fix value during attack
            print("ATTACK (fix = n/a): {}, {}, {}, {} [count = {}]".format(ll_ts, lat, self.lon_constant, ll_hgt, self.attack_count))
            return spoofer_msg


    # Spoofer LLH after attack success and RX is locked to spoofer signal
    def spoof_lock_location_update(self, loc):
        lat = loc[1]
        lon = loc[2]
        hgt = 10.0  # loc[3]

        if self.SL:  # Locked onto the spoofing signal
            rx_vel = self.get_rx_velocity(loc)
            self.rpi_msg_last_lock = loc  # update most recent RX position fix

            # v_lat = rx_vel[0]
            # v_lon = rx_vel[1]
            # v_hgt = rx_vel[2]
            elapsed_time = rx_vel[3]

            self.lat_constant = self.lat_constant + (self.v_step * elapsed_time)

            spoofer_msg = [0, self.lat_constant, self.lon_constant, hgt]
            self.send("{},{},{},{}".format(0, self.lat_constant, self.lon_constant, hgt), self.gps_sdr_sim_addr)
            print("ATTACK (fix = 3D-S) {}, {}, {}, {}".format(0, self.lat_constant, self.lon_constant, hgt))
            return spoofer_msg



    # Spoofer LLH after RX locked onto spoofing signal, and transmitting a new location change
    def deviation_location_update(self, dev_llh):
        # lat = dev_llh[1]
        # lon = dev_llh[2]
        hgt = 10.0  # dev_llh[3]

        if dev_llh[4] == 3:
            rx_vel = self.get_rx_velocity(dev_llh)
            self.rpi_msg_last_lock = dev_llh  # update most recent RX position fix

            v_lat = rx_vel[0]
            v_lon = rx_vel[1]
            v_hgt = rx_vel[2]
            elapsed_time = rx_vel[3]

            # self.lat_constant = self.lat_constant + (self.v_step * elapsed_time)
            self.lon_constant = self.lon_constant + (self.v_step * elapsed_time)

            spoofer_msg = [0, self.lat_constant, self.lon_constant, hgt]
            self.send("{},{},{},{}".format(0, self.lat_constant, self.lon_constant, hgt), self.gps_sdr_sim_addr)
            print("ATTACK: DEVIATION (fix = 3D-S) {}, {}, {}, {}".format(0, self.lat_constant, self.lon_constant, hgt))
            return spoofer_msg

    def adjust_power(self, target, value):
        self.send("{},{},{}".format(target, value, self.rpi_msg[5]), self.usrp_addr)  # self.rx_elapsed

    def send(self, msg, addr):
        self.sock.sendto(bytes(msg, "utf-8"), addr)  # addr = (ip, port)


class SpooferConsole(pyqtgraph.console.ConsoleWidget):
    def __init__(self, parent=None, namespace=None, historyFile=None, text=None, editor=None, spoofer=None):
        # type: (object, object, object, object, object, object) -> object
        self.spoofer = spoofer
        super().__init__(parent, namespace, historyFile, text, editor)

    def execSingle(self, cmd):
        try:
            # output = eval(cmd, self.globals(), self.locals())
            output = self.spoofer.process_user_input(cmd)
            self.write(repr(output))  # + '\n')
        except SyntaxError:
            try:
                exec (cmd, self.globals(), self.locals())
            except SyntaxError as exc:
                if 'unexpected EOF' in exc.msg:
                    self.multiline = cmd
                else:
                    self.displayException()
            except:
                self.displayException()
        except:
            self.displayException()

def update_plot(x, y, c, d, rx_df, authentic_df, spoofer_df):
    if spoofer.rpi_msg[4] == 3:
        x['victim'].append(round(spoofer.rpi_msg[2], 6))
        y['victim'].append(round(spoofer.rpi_msg[1], 6))
        c['victim'].setData(x['victim'], y['victim'])

        d['rx_lat'].append(round(spoofer.rpi_msg[1], 6))
        d['rx_lon'].append(round(spoofer.rpi_msg[2], 6))

    if spoofer.spoofer_msg[2] != 0:
        if (spoofer.spoofer_msg[1] != 0) & (spoofer.spoofer_msg[1] < 48):
            x['spoofer'].append(round(spoofer.spoofer_msg[2], 6))
            y['spoofer'].append(round(spoofer.spoofer_msg[1], 6))
            c['spoofer'].setData(x['spoofer'], y['spoofer'])

            d['spoofer_lat'].append(round(spoofer.spoofer_msg[1], 6))
            d['spoofer_lon'].append(round(spoofer.spoofer_msg[2], 6))

    if spoofer.authentic_msg[2] != 0:
        if spoofer.authentic_msg[1] != 0:
            x['authentic'].append(round(spoofer.authentic_msg[2], 6))
            y['authentic'].append(round(spoofer.authentic_msg[1], 6))
            c['authentic'].setData(x['authentic'], y['authentic'])

            d['authentic_lat'].append(round(spoofer.authentic_msg[1], 6))
            d['authentic_lon'].append(round(spoofer.authentic_msg[2], 6))



if __name__ == "__main__":
    spoofer = Spoofer(host=HOST, port=PORT)
    spoofer.set_gps_sdr_sim_addr((gps_sdr_sim_IP, gps_sdr_sim_port))
    spoofer.set_usrp_addr((usrp_IP, usrp_port))
    spoofer.set_trajectory_map_addr((trajectory_map_IP, trajectory_map_port))

    rx_df = pd.DataFrame(columns=['latitude', 'longitude', 'color', 'size', 'label'])
    authentic_df = pd.DataFrame(columns=['latitude', 'longitude', 'color', 'size', 'label'])
    spoofer_df = pd.DataFrame(columns=['latitude', 'longitude', 'color', 'size', 'label'])

    file = open('map/data.csv', 'w+')
    file.close()

    name = 'GPS Spoofing Console'
    spoofer.start()
    text = """
    'a'  : start spoofing attack
    'r'  : reset signal parameters back to default
    'd'  : begin location deviation
    'n'  : set noise = 0, spoofing gain = 0

    Enter a command:
    """

    app = QtGui.QApplication([])
    mw = QtGui.QMainWindow()
    mw.setWindowTitle(name)
    cw = QtGui.QWidget()

    mw.resize(300, 400)
    mw.setCentralWidget(cw)

    l = QtGui.QVBoxLayout()

    cw.setLayout(l)
    # pw = pg.PlotWidget(name='LLH')
    # l.addWidget(pw)
    c = SpooferConsole(text=text, spoofer=spoofer)
    l.addWidget(c)

    mw.show()

    # curves['victim'] = pw.plot(pen=pg.mkPen('y', width=1),name='VICTIM',symbolBrush=(126, 47, 142),symbolPen='y',symbol='+')
    # curves['spoofer'] = pw.plot(pen=pg.mkPen('r', width=1),name='SPOOFER',symbolBrush=(126, 47, 142),symbolPen='r',symbol='+')
    # curves['authentic'] = pw.plot(pen=pg.mkPen('g', width=1),name='AUTHENTIC',symbolBrush=(126, 47, 142),symbolPen='g',symbol='+')

    # timer = QtCore.QTimer()
    # timer.timeout.connect(lambda: update_plot(x_data, y_data, curves, data, rx_df, authentic_df, spoofer_df))
    # timer.start(100)  # ms
    app.instance().exec_()

