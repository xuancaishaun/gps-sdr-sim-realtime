# This script implements frequently used functions.

from geopy import distance, Point

def compute_destination(src, dist_km, bearing_deg):
    point = Point(src[0], src[1])
    dist = distance.distance(kilometers=dist_km)
    dst = dist.destination(point, bearing_deg, dist)

    if len(src) == 2:  # (lat, lon)
        return (dst.latitude, dst.longitude)
    elif len(src) == 3:  # (lat, lon, hgt)
        return (dst.latitude, dst.longitude, src[2])

# Calculate the geodesic distance between two points.
# Default ellipsoidal model: ‘WGS-84’
def compute_dist_km(pos_lat_lon1, pos_lat_lon2):
    return distance.distance(pos_lat_lon1, pos_lat_lon2).km

def knots_to_km_hr(knots):
    return knots*1.852

def knots_to_km_sec(knots):
    return knots*0.000514444


if __name__ == '__main__':
    # point_x = (41.490082, -71.31270)
    # point_y = (41.49008, -71.31271)

    # delay = 0
    # point_x = (47.65370597, -122.30631848)
    # point_y = (47.65370491, -122.30624807)

    # delay = 1
    # point_x = (47.65363941, -122.30624993)
    # point_y = (47.65358856, -122.31045337)

    # delay = 5
    # point_x = (47.65371001, -122.30631574)
    # point_y = (47.65368487, -122.32716137)

    # delay = 0.5
    # point_x = (47.65368704, -122.30629695)
    # point_y = (47.65371434, -122.30837439)

    # delay = 0.1
    # point_x = (47.65369198, -122.30629804)
    # point_y = (47.65371398, -122.30675013)

    delay = 10
    point_x = (47.65370858, -122.30625457)
    point_y = (47.62034073, -122.27817126)


    # 600 knots is 1111.2 km/h or 308.666667 m/sec
    curr_pos = point_x
    #dist_km = knots_to_km_sec(600)*1
    dist_km = 0.001   # 0.001 km ~ 1e-5 deg
    bearing_deg = 0
    next_pos = compute_destination(curr_pos, dist_km, bearing_deg)
    print('> novatel static switch [{} sec delay]'.format(delay))
    print("> computed distance [km] = {}\n\n".format(compute_dist_km(point_x, point_y)))


    print('Curr pos is {}'.format(curr_pos))
    print('Next pos is {}: dist = {} km, bearing = {} deg'.format(next_pos, dist_km, bearing_deg))
    print('Geodesic distance = {}'.format(compute_dist_km(curr_pos, next_pos)))